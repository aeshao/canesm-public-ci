from abc import ABC, abstractmethod

class experiment(ABC):
  """Holds information about a given experiment
  :param name: Name of the experiment
  :type name: string
  :param exe_path: Dictionary executables associated with this experiment
  :type exe_path: dict
  :param atmosphere_sizes_file: File containing CPP definitions controlling CanAM array sizes
  :type atmosphere_sizes_file: string
  :param atmosphere_config_file: File containing CPP controlling the CanAM configuraiton
  :type atmosphere_config_file: string
  """

  name = None
  exe_paths = {
    'atmosphere':None,
    'ocean':None,
    'coupler':None
  }
  is_coupled = False

class coupled_experiment(experiment):
  is_coupled = True
  atmosphere_sizes_file = None
  atmosphere_config_file = None
  ocean_config = 'CCC_CANCPL_ORCA1_LIM_CMOC'

  def __init__(self, platform):
    self.exe_paths['atmosphere'] = f'{platform.atmosphere_source}/build/bin/AGCM.{self.name}'
    self.exe_paths['coupler'] = f'{platform.coupler_source}/build/CPL.{self.name}'
    self.exe_paths['ocean'] = f'{platform.ocean_source}/nemo/CONFIG/{self.ocean_config}/nemo.exe.{self.name}'

class piControl(coupled_experiment):
  name = 'piControl'
  atmosphere_sizes_file = 'build/include/.defaults/cppdef_sizes.h'
  atmosphere_config_file = 'build/include/.defaults/cppdef_config.h'

  def __init__(self, platform):
    self.atmosphere_sizes_file = f'{platform.atmosphere_source}/{self.atmosphere_sizes_file}'
    self.atmosphere_config_file = f'{platform.atmosphere_source}/{self.atmosphere_config_file}'
    super().__init__(platform)

experiments = {
  'piControl':piControl
}
