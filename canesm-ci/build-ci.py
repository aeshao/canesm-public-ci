from builder import builder
import platform
import experiment
import argparse
import time
import shutil
import os

def main( args ):

  # Initialize the platform instance
  this_platform = platform.supported_platforms[args.platform](
    args.canesm_source,
    args.num_atmosphere_nodes,
    args.num_ocean_ranks
  )

  # Parse and initialize the requested experiments to run
  requested_experiments = args.experiments.split(' ')
  experiments = [ experiment.experiments[exp](this_platform) for exp in requested_experiments ]
  builds = [ builder(this_platform, exp) for exp in experiments ]

  # Build each executable for every experiment
  start = time.time()
  for build in builds:
    build.build_stages()

  # Copy the builds to a cached directory
  for exp in experiments:
    exp_path = f'{args.cache_dir}/{exp.name}/'
    os.makedirs(exp_path, exist_ok=True)
    for exe in exp.exe_paths.values():
      shutil.copy(exe, exp_path)

  print(f'All builds completed in: {time.time()-start:.2f} seconds')

if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Handles the build stage of the Gitlab CI')
  parser.add_argument('cache_dir', help='Path to the Gitlab CI cache')
  parser.add_argument('canesm_source', help='The path where the CanESM source code is located')
  parser.add_argument('platform', help='The identifier for the platform to run on')
  parser.add_argument('experiments', help='Experiment names to run')
  parser.add_argument('num_atmosphere_nodes', help='Number of atmosphere nodes')
  parser.add_argument('num_ocean_ranks', help='Number of ocean MPI ranks')

  args = parser.parse_args()
  main(args)