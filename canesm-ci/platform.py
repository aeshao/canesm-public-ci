from abc import ABC, abstractmethod

class platform(ABC):
  """Holds platform specific configuration options

  :param name: The name of the platform (e.g. banting, google-compute)
  :type name: string
  :param data_directory: The root path where inputs for CanESM experiments are stored
  :type data_directory: string
  :param experiments: A list of experiments to test on this platform
  :type  experiments: list
  :param compilers: Dictionary containing the compilers on this platform
  :type compilers: dict
  env_vars = {}

  :type ABC: [type]
  """
  name = 'generic'
  data_directory = None
  nemo_arch = None
  mkmf_template = None
  make_cmd = 'make'
  cache_directory = '/tmp/'

  def __init__(self, canesm_source, num_atmosphere_nodes, num_ocean_ranks):
    self.canesm_source = canesm_source
    self.coupler_source = f'{canesm_source}/CanCPL'
    self.atmosphere_source = f'{canesm_source}/CanAM'
    self.ocean_source = f'{canesm_source}/CanNEMO'
    self.num_atmosphere_nodes = num_atmosphere_nodes
    self.num_ocean_ranks = num_ocean_ranks

class gcp_ubuntu_gnu(platform):
  """Defines all platform-specific options for running an Ubuntu 18.04 container using GNU compilers
  """

  name = 'gcp_ubuntu_gnu'
  data_directory = '/home/ashao/data'
  compilers = {
    'fortran':'mpif90',
    'c++':'mpicxx',
    'c':'mpicc'
  }
  nemo_arch = 'gfortran_linux'
  mkmf_template = 'mkmf.gcc'

  def __init__(self, *args):
    super().__init__(*args)

supported_platforms = {
  'gcp_ubuntu_gnu':gcp_ubuntu_gnu
}
