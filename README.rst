CanESM CI
---------

This repository contains the Python tools that are used to drive
the continuous integration on Gitlab. The python repository has
no dependencies other than Python 3.8.

Supported Platforms
===================

The only supported platform is on Google Compute Cloud. Additional
platforms can be defined in ``platform.py``.
