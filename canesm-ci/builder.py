from abc import ABC, abstractmethod
import fileinput
import subprocess
import shutil
import time
import os

class stage(ABC):
  """ Defines actions that need to be taken to build a specific stage of the overall configuration

  :param name: The name of the stage
  :type name: string

  """

  name = None

  def __init__(self):
    """Sets up the configuration stage

    """
    pass

  @abstractmethod
  def build(self):
    """Performs the actual build of the current stage

    """
    pass

class null_stage(stage):
  """A null stage type that does nothing
  """
  name = None
  def build(self):
    return

class ocean_stage(stage):
  """Builds the ocean component of CanESM
    :param coupled: If true, this is part of a coupled experiment
    :type coupled: bool
    :param arch_name: Name of the ARCH file to use

  """

  def build(self, platform, experiment):
    if experiment.is_coupled:
      external_path = f'{platform.ocean_source}/nemo/NEMO/external'
      # Manually modify com_cpl to accomodate the need for .h90 extension
      com_cpl_dest = f'{platform.coupler_source}/src/comm/com_cpl.F90'
      shutil.copy( com_cpl_dest, f'{external_path}')
      output = subprocess.run( [
        f"sed -i 's/cppdef_sizes.h/cppdef_sizes.h90/' {external_path}/com_cpl.F90"],
        shell=True
      )
      output = subprocess.run( [
        f"sed -i 's/cppdef_config.h/cppdef_config.h90/' {external_path}/com_cpl.F90"],
        shell=True
      )

      cpp_config_file = experiment.atmosphere_config_file
      cpp_sizes_file = experiment.atmosphere_sizes_file
      shutil.copy(f'{cpp_config_file}', f'{external_path}/cppdef_config.h90')
      shutil.copy(f'{cpp_sizes_file}',  f'{external_path}/cppdef_sizes.h90')

    output = subprocess.run(' '.join([
      './makenemo',
      f'-j {os.cpu_count()}',
      f'-n {experiment.ocean_config}',
      f'-m {platform.nemo_arch}']),
      cwd=f'{platform.ocean_source}/nemo/CONFIG/',
      capture_output=True,
      shell=True,
      text=True)
    build_directory = f'{platform.ocean_source}/nemo/CONFIG/{experiment.ocean_config}/BLD/bin'

    if (os.path.exists(f'{build_directory}/nemo.exe')):
      shutil.copy(f'{build_directory}/nemo.exe',experiment.exe_paths['ocean'])
    else:
      print(output.stdout)
      print(output.stderr)
      raise Compile_Error_Ocean

class atmosphere_stage(stage):
  """Builds the atmospheric component of CanESM
  """

  def build(self, platform, experiment):

    cpp_config_file = experiment.atmosphere_config_file
    cpp_sizes_file = experiment.atmosphere_sizes_file
    build_target = f'AGCM.{experiment.name}'
    make_arguments = ' '.join([
      '-j',
      f'MKMF_TEMPLATE={platform.mkmf_template}',
      f'CPP_CONFIG_FILE={cpp_config_file}',
      f'CPP_SIZES_FILE={cpp_sizes_file}',
      f'AGCM_EXE={build_target}'
    ])

    output = subprocess.run( [
      f"sed -i 's/#define.*_PAR_NNODE_A.*/#define _PAR_NNODE_A {platform.num_atmosphere_nodes}/' {cpp_sizes_file}"
    ], shell=True)

    shutil.copy(cpp_config_file, f'{platform.atmosphere_source}/build')
    shutil.copy(cpp_sizes_file, f'{platform.atmosphere_source}/build')

    output = subprocess.run(' '.join([platform.make_cmd,make_arguments]),
                             cwd = f'{platform.atmosphere_source}/build',
                             capture_output=True, text=True, shell=True)

    if output.returncode != 0:
      print(output.stderr)
      raise Compile_Error_Atmosphere

class coupler_stage(stage):
  """Builds the coupler used in CanESM
  """
  def build(self, platform, experiment):

    build_target = f'CPL.{experiment.name}'
    make_arguments = ' '.join([
      f'MKMF_TEMPLATE={platform.mkmf_template}',
      f'CPL_EXE={build_target}'
    ])

    shutil.copy(
      f'{experiment.atmosphere_config_file}',
      f'{platform.coupler_source}/build'
    )

    shutil.copy(
      f'{experiment.atmosphere_sizes_file}',
      f'{platform.coupler_source}/build'
    )

    output = subprocess.run(' '.join([platform.make_cmd,make_arguments]),
      cwd = f'{platform.coupler_source}/build', capture_output=True, shell=True, text=True)

    if output.returncode != 0:
      print(output.stderr, flush=True)
      raise Compile_Error_Coupler

class builder:
  """ Builds a specific configuration of CanESM or its components

  :param stages: Holds initialized stages of the build process
  :type stages: dict
  """

  stages = {
    'atmosphere' : null_stage,
    'ocean' : null_stage,
    'coupler' :null_stage
  }

  def __init__(self, platform, experiment):
    self.platform = platform
    self.experiment = experiment
    if experiment.is_coupled:
      self.stages['atmosphere'] = atmosphere_stage()
      self.stages['coupler'] = coupler_stage()
      self.stages['ocean'] = ocean_stage()

  def build_stages(self):
    print(f'Building executables for {self.experiment.name}')
    for key, stage in self.stages.items():
      print(f'\tBuilding {key}...',flush=True)
      start = time.time()
      stage.build(self.platform, self.experiment)
      print(f'\t\tCompleted in {time.time()-start:.2f} seconds',flush=True)

class Compile_Error_Atmosphere(Exception):
  """ Raised when the AGCM fails to compile
  """
  pass

class Compile_Error_Coupler(Exception):
  """ Raised when the coupler fails to compile
  """
  pass

class Compile_Error_Ocean(Exception):
  """ Raised when the ocean fails to compile
  """
  pass



